# Hi there 👋, I am Daniel López.
I am student of Mechatronics Engineer and passionate about robotics.

:robot: :computer: :book:

**About me...**
* Member of RAS IEEE Javeriana since february 2021.
* President of RAS IEEE Javeriana since july 2022.

:books: :computer:

**Currently learning...**
* MoveIt.
* More about ROS. 
* More about Python.
* OpenCV

:robot: :bust_in_silhouette:

**Languages:**
<br/>
<img align="left" alt="C++" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/306px-ISO_C%2B%2B_Logo.svg.png" />
<img align="left-float" alt="Python" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1024px-Python-logo-notext.svg.png" />
<br/>

**Tools and environments:**
<br/>
<img align="left" alt="ROS" height="30px" src="https://camo.githubusercontent.com/8fbe45b3aa44949d5cb3ce7619f1e7bb3bea0630/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f622f62622f526f735f6c6f676f2e737667" />
<img align="left" alt="Visual Studio Code" height="30px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1024px-Visual_Studio_Code_1.35_icon.svg.png" />
&nbsp;
<img align="left" alt="MoveIt" height="30px" padding-right="10px" src="http://docs.ros.org/en/kinetic/api/moveit_tutorials/html/_static/logo.png" />
<br/>

